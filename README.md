# React Boilerplate
[![ReactJS](https://i.imgur.com/Cka06kf.png)](https://reactjs.org/)
[![Pipeline](https://gitlab.com/MihajloNesic/react-boilerplate/badges/master/build.svg)](https://gitlab.com/MihajloNesic/react-boilerplate/commits/master)
[![create-react-app](https://badge.fury.io/js/create-react-app.svg)](https://facebook.github.io/create-react-app/)

Simple React boilerplate for GitLab Pages<br>
#### [LIVE DEMO](https://mihajlonesic.gitlab.io/react-boilerplate/)

##### Other demos
[React Todo List](https://mihajlonesic.gitlab.io/react-todo-list/)<br>
[React Markdown Editor](https://mihajlonesic.gitlab.io/react-markdown/)

## Getting Started

Clone the boilerplate if you haven't downloaded it
```
git clone https://gitlab.com/MihajloNesic/react-boilerplate.git
```

Navigate to the boilerplate project folder
```
cd react-boilerplate
```

Run the setup to get everything up and running
```
npm install
```

Inside **package.json**, change the *homepage* to your own URL
```
"homepage": "http://USERNAME.gitlab.io/PROJECT"
```

And start the development server
```
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Alteriative

**Fork** this project

Inside **package.json**, change the *homepage* to your own URL
```
"homepage": "http://USERNAME.gitlab.io/PROJECT"
```

## More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).